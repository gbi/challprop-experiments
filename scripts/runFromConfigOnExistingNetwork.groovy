/**
 * @author vveitas
 * run the similar simulations to the ones Evo parformed on matlab just to
 * do the comparative benchmarking and also see differences in results
 *
 */

package org.globalbraininstitute.challprop.experiments

import org.globalbraininstitute.challprop.global.Globals
import org.globalbraininstitute.challprop.mocks.*
import java.io.File
import java.nio.file.Files
import java.nio.file.StandardCopyOption

import java.lang.management.ThreadMXBean
import java.lang.management.ManagementFactory

import groovy.util.logging.Log4j
import org.apache.log4j.PropertyConfigurator

scriptDir = new File(getClass().protectionDomain.codeSource.location.path).parent
scriptFile = getClass().protectionDomain.codeSource.location.path

println scriptDir
println scriptFile

//loading log4j properties
def config = new ConfigSlurper().parse(new File('log4j.groovy').toURL())
PropertyConfigurator.configure(config.toProperties())

tempConfigFileName = "../configs/challprop.conf"
Map parameters = new ConfigSlurper('config').parse(new File(tempConfigFileName).toURI().toURL())

println ((new Date()).toString() + ": Running simulation with " + parameters.numberOfAgents + " agents:")

ThreadMXBean bean = ManagementFactory.getThreadMXBean( );
simulationStartWall = System.nanoTime()
simulationStartCPU = bean.getCurrentThreadCpuTime()

parameters.simulationStartWall = simulationStartWall
parameters.simulationStartCPU = simulationStartCPU

numberOfSituations = (parameters.numberOfAgents*parameters.believe).toInteger()

// print "Creating non-connected network of "+parameters.numberOfAgents+" agents.."
ts = System.currentTimeMillis()
//def agentList = NetworkMocks.createNetworkMock6(parameters.numberOfAgents)
println "Getting agents form the existing network"
def agentList = Globals.network.getFramedAgentsList()
println "Existing network has " + agentList.size() + " agents."
agentListsSmall = agentList.collate(2000)

println "....Done. In "+(System.currentTimeMillis() - ts)/1000+" seconds."
for (int iteration = 0;iteration < parameters.numberOfCycles;iteration++) {
	print "Iteration "+iteration+": generating "+numberOfSituations+" new situations.."
	ts = System.currentTimeMillis()
	ControlledSimulation.generation(numberOfSituations)
	println "....Done. In "+(System.currentTimeMillis() - ts)/1000+" seconds."
	print "Iteration "+iteration+": propagating challenges and new situations.."
	ts = System.currentTimeMillis()
	agentListsSmall.each{agentListSmall ->
		ControlledSimulation.propagation(agentListSmall,1)
	}
	println "....Done. In "+(System.currentTimeMillis() - ts)/1000+" seconds."
}

parameters.simulationFinishWall = System.nanoTime()
parameters.simulationFinishCPU = bean.getCurrentThreadCpuTime()

print ((new Date()).toString() + ": Archiving the graph...")
commitNo = ""
new File("../experiment.version").eachLine{commitNo = it}
new File(parameters.pathOnDisk+commitNo).mkdirs()
Globals.network.g.saveGraphSON(parameters.pathOnDisk+commitNo+"/graph.graphson")
println "...Done"

parameters.archivingFinishedWall =System.nanoTime()
parameters.archivingFinishedCPU = bean.getCurrentThreadCpuTime()

print ((new Date()).toString() + " :Closing cassandra database..")
//Globals.network.clear()
// not using this because it runs out of memory for large(r) simulations
Globals.network.gracefulShutdown()
println "...Done"

parameters.deletingCassandraWall =System.nanoTime()
parameters.deletingCassandraCPU = bean.getCurrentThreadCpuTime()

File file = new File(parameters.pathOnDisk+commitNo+"/parameters.conf")
file.createNewFile()
file.withWriter{ writer ->
    parameters.writeTo( writer )
    writer.close()
}

Files.copy(new File("../code.version").toPath(),new File(parameters.pathOnDisk+commitNo+"/code.version").toPath())
Files.copy(new File("../experiment.version").toPath(),new File(parameters.pathOnDisk+commitNo+"/experiment.version").toPath())
Files.copy(new File("/var/log/challprop/challprop.log").toPath(),new File(parameters.pathOnDisk+commitNo+"/challprop.log").toPath())
