/**
 * @author vveitas
 * run the similar simulations to the ones Evo parformed on matlab just to 
 * do the comparative benchmarking and also see differences in results
 *
 */

package org.globalbraininstitute.challprop.experiments

import org.globalbraininstitute.challprop.global.Globals
import org.globalbraininstitute.challprop.mocks.*
import java.io.File
import java.nio.file.Files
import java.nio.file.StandardCopyOption

import java.lang.management.ThreadMXBean
import java.lang.management.ManagementFactory


def ConfigObject config = new ConfigObject()
config.properties = [:]
config.properties.numberOfAgents =args[0].toInteger()
config.properties.numberOfIterations = 100
config.properties.believe=0.2 // percentage of agents getting god's challenges each iteration (according to Evo's parameters)

println ((new Date()).toString() + ": Running simulation with " + config.properties.numberOfAgents + " agents:")

ThreadMXBean bean = ManagementFactory.getThreadMXBean( );
simulationStartWall = System.nanoTime()
simulationStartCPU = bean.getCurrentThreadCpuTime()

config.properties.simulationStartWall = simulationStartWall
config.properties.simulationStartCPU = simulationStartCPU

numberOfSituations = (config.properties.numberOfAgents*config.properties.believe).toInteger()

print "Creating non-connected network of "+config.properties.numberOfAgents+" agents.."
ts = System.currentTimeMillis()
def agentList = NetworkMocks.createNetworkMock6(config.properties.numberOfAgents)
println "....Done. In "+(System.currentTimeMillis() - ts)/1000+" seconds."
for (int iteration = 0;iteration < config.properties.numberOfIterations;iteration++) {
	print "Iteration "+iteration+": generating "+numberOfSituations+" new situations.."
	ts = System.currentTimeMillis()
	ControlledSimulation.generation(numberOfSituations)
	println "....Done. In "+(System.currentTimeMillis() - ts)/1000+" seconds."
	print "Iteration "+iteration+": propagating challenges and new situations.."
	ts = System.currentTimeMillis()
	ControlledSimulation.propagation(agentList,1)
	println "....Done. In "+(System.currentTimeMillis() - ts)/1000+" seconds."
}

config.properties.simulationFinishWall = System.nanoTime()
config.properties.simulationFinishCPU = bean.getCurrentThreadCpuTime()

print ((new Date()).toString() + ": Archiving the graph...")
commitNo = ""
new File("../experiment.version").eachLine{commitNo = it}
new File("../../data/"+commitNo).mkdirs()
Globals.network.g.saveGraphSON("../../data/"+commitNo+"/graph.graphson")
//g = TitanFactory.open("../../data/"+commitNo+"/titan")
//g.loadGraphSON("../../data/"+commitNo+"/graph.graphson")
//g.shutdown()
println "...Done"

config.properties.archivingFinishedWall =System.nanoTime()
config.properties.archivingFinishedCPU = bean.getCurrentThreadCpuTime()

print ((new Date()).toString() + " :Closing cassandra database..")
//Globals.network.clear()
// not using this because it runs out of memory for large(r) simulations
Globals.network.gracefulShutdown()
println "...Done"

config.properties.deletingCassandraWall =System.nanoTime()
config.properties.deletingCassandraCPU = bean.getCurrentThreadCpuTime()

File file = new File("../../data/"+commitNo+"/parameters.conf")
file.createNewFile()
file.withWriter{ writer ->
    config.writeTo( writer )
    writer.close()
}

