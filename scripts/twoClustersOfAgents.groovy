/**
 * Create a two clusters of agents:
 * First cluster has needVector with large values - these agents will on average get less benefit from processing challenges
 * Second cluster has needVector with small values - these agents will process anything that has intensity a little higher
 * than zero .The average benefit of first cluster should be considerably smaller than second
 * Inter and intra cluster link numbers and link strengths should also be different
 * @author vveitas
 *
 */

package org.globalbraininstitute.challprop.experiments

import org.globalbraininstitute.challprop.global.Globals
import org.globalbraininstitute.challprop.mocks.*
import org.globalbraininstitute.challprop.utils.*
import java.nio.file.Files
import org.globalbraininstitute.challprop.global.Parameters

import com.thinkaurelius.titan.core.TitanFactory

Globals.network.godVertex.setProperty('simulationStart',System.nanoTime())

def parameters = new HashMap()
parameters.put("c1", new HashMap())
parameters.c1.put("agents",100)
parameters.c1.put("needVector",Utils.serializeVector(Algorithms.newPresetVector(100.0)))

parameters.put("c2", new HashMap())
parameters.c2.put("agents",100)
parameters.c2.put("needVector",Utils.serializeVector(Algorithms.newPresetVector(1.0)))

def agentList = NetworkMocks.createNetworkMockWithClusters(parameters)
(0..10).each{
	ControlledSimulation.generation(10)
	ControlledSimulation.propagation(agentList,10)
}

Globals.network.godVertex.setProperty('simulationFinish',System.nanoTime())

println "Experiment ID: " + Globals.network.global.simulationID

print "Archiving the graph..."
commitNo = ""
new File("../experiment.version").eachLine{commitNo = it}
new File("../../data/"+commitNo).mkdirs()
Globals.network.g.saveGraphSON("../../data/"+commitNo+"/graph.graphson")
g = TitanFactory.open("../../data/"+commitNo+"/titan")
g.loadGraphSON("../../data/"+commitNo+"/graph.graphson")
g.shutdown()
println "Done"

print "Flushing Cassandra database on disk..."
Globals.network.clear()
Globals.network.gracefulShutdown()
println "Done"

print "Copying log file to the graph directory of the experiment"
Files.copy(new File("/var/log/challprop/challprop.log").toPath(),new File("../../data/"+commitNo+"/challprop.log").toPath())
println "Done"