log4j {
    rootLogger="DEBUG, ChallpropFileAppender"
    logger."gbi.challprop"="DEBUG, ChallpropFileAppender"
    additivity."gbi.challprop"=false
    appender.ChallpropFileAppender = "org.apache.log4j.FileAppender"
    appender."ChallpropFileAppender.File"="/var/log/challprop/challprop.log"
    appender."ChallpropFileAppender.Append"=false
    appender."ChallpropFileAppender.layout"="org.apache.log4j.PatternLayout"
    appender."ChallpropFileAppender.layout.ConversionPattern"= "%-4r %d [%t] %-5p %c %x : %m%n"
}
