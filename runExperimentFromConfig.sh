#!/bin/sh
cd ..
WORKDIR=$(pwd)
EXPDIR=$WORKDIR/challprop-experiments
CODEDIR=$WORKDIR/challprop

# get the code commit hash
cd $CODEDIR
git rev-parse HEAD> $EXPDIR/code.version

#git commit experiment
cd $EXPDIR
git add -A
git commit -m "$(date)"
git rev-parse HEAD> $EXPDIR/experiment.version

# starting titan cassandra
$WORKDIR/challprop/configs/titan_cassandra_start.sh
wait && sleep 5

cd $EXPDIR/scripts
groovy runFromConfigOnExistingNetwork.groovy
# groovy runFromConfig.groovy

# stopping titan cassandra
$WORKDIR/challprop/configs/titan_cassandra_stop.sh
wait && sleep 1

# and deleting it from the disk
# rm -r /media/ramdisk/cassandra # can be commented out in case the script throws exceptions during run time...
# this is needed for large simulations when g.V.remove() throws memory exception
wait && sleep 1
