#!/bin/sh
cd ..
WORKDIR=$(pwd)
EXPDIR=$WORKDIR/challprop-experiments
CODEDIR=$WORKDIR/challprop

# get the code commit hash
cd $CODEDIR
git rev-parse HEAD> $EXPDIR/code.version

#git commit experiment
cd $EXPDIR
git add -A
git commit -m "autocommit before running experiment"
git rev-parse HEAD> $EXPDIR/experiment.version	

cd $EXPDIR/scripts
groovy twoClustersOfAgents.groovy

