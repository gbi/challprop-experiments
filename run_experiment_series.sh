#!/bin/sh
cd ..
WORKDIR=$(pwd)
EXPDIR=$WORKDIR/challprop-experiments
CODEDIR=$WORKDIR/challprop

//for AGENTS in 10 20 30 40 50 60 70 80 90 100 200 400 600 800 1000 5000 10000
for AGENTS in 6000
do
	# get the code commit hash
	cd $CODEDIR
	git rev-parse HEAD> $EXPDIR/code.version

	#git commit experiment
	cd $EXPDIR
	git add -A
	git commit -m "benchmarking with $AGENTS agents: cputime + walltime"
	git rev-parse HEAD> $EXPDIR/experiment.version

	# starting titan cassandra
	$WORKDIR/challprop/configs/titan_cassandra_start.sh
	wait && sleep 5
	
	cd $EXPDIR/scripts
	groovy runFromConfig.groovy $AGENTS
	
	# stopping titan cassandra
	$WORKDIR/challprop/configs/titan_cassandra_stop.sh
	wait && sleep 1
	
	# and deleting it from the disk
	rm -r /media/ramdisk/cassandra
	# this is needed for large simulations when g.V.remove() throws memory exception
	wait && sleep 1
	
done
