Scripts and files for running experiments on [challprop](https://bitbucket.org/gbi/challprop/) framework.

The way to run experiment is to execute bash scripts in the main directory. These scripts automatically commit the whole directory to the git repo, so that the hash of the commit can be used in analysis to connect to the exact scripts that were used for it.

# Prerequisites / dependencies 

## Groovy version

Note, that special attention should be paid to the groovy version compatibility in the environment. The code is tested to work with groovy 2.3.10, which is indicated in main pom.xml. The system wide groovy installation should be of the same version, which is done by:

```
$ curl -s get.sdkman.io | bash
$ source "$HOME/.sdkman/bin/sdkman-init.sh"
$ sdk install groovy 2.3.10
$ groovy -version
```

It may be a good idea to reboot after that or at least check groovy version before running script - it may produce cryptic errors if versions do not match (saves a lot of headache).

## Titan Graph Database

[challprop](https://bitbucket.org/gbi/challprop/) depends on the [Titan Graph Database](http://thinkaurelius.github.io/titan/), version 0.4.4. If you do not have this (chances are that you do not), then:

* Create a new directory, e.g. `/media/data/opt/` and change to it;
* Download [titan-server-0.4.4](http://s3.thinkaurelius.com/downloads/titan/titan-server-0.4.4.zip) by `wget http://s3.thinkaurelius.com/downloads/titan/titan-server-0.4.4.zip`;
* And unpack it into the same directory: `unzip titan-server-0.4.4.zip`;
* That is all almost all - the rest is done via scripts (some changes to them may be needed to them too, if you use another path than indicated).

It is also a good idea is to increase java heap before running - often the heap runs out in the middle of the simulation or at the time of dumping the graph from cassandra to file. Probably a lousy optimisation parameters, but unless you know how to tweak them - increase java hep by `export JAVA_OPTS = "-Xmx8g"` (or something in that style) in /etc/profile. Another option is to put `JAVA_OPTS = "-Xmx8g"` in cassandra start script.